#!/bin/bash
#
# Set up for Yale Haskell 2.x users.
#

export PRELUDE="${HASKELL}/progs/prelude"
export HASKELL_LIBRARY="${HASKELL}/progs/lib"

# You may need to change this to point at the appropriate subdirectory,
# depending on which Lisp is being used.
export PRELUDEBIN="${PRELUDE}/clisp"

# You may need to change this to point at the appropriate executable,
# depending on which Lisp is being used.
export HASKELLPROG="${HASKELL}/bin/clisp-haskell"
