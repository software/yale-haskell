PREFIX=/
SHARE=$(PREFIX)/share/yale-haskell
DIRS=					\
	cfn					\
	com					\
	flic				\
	derived				\
	command-interface	\
	type				\
	printers			\
	cl-support			\
	progs/prelude		\
	csys				\
	prec				\
	depend				\
	import-export		\
	runtime				\
	backend				\
	util				\
	parser				\
	top					\
	tdecl				\
	support				\
	ast


build:
	sh ./build-clisp-haskell.bash

install-doc:
	install -d $(SHARE)/doc
	cp -ar ./doc $(SHARE)

install-examples:
	install -d $(SHARE)
	cp -ar ./progs/demo $(SHARE)

install-lisp:
	for dir in $(DIRS); do \
		install -d $(SHARE)/$$dir; \
		cp -ar $$dir/clisp $(SHARE)/$$dir; \
	done
	install command-interface-help $(SHARE)

install: install-doc install-lisp install-examples
	install -d $(PREFIX)/bin
	install ./bin/clisp-haskell $(PREFIX)/bin
	echo "#!/bin/sh" > $(PREFIX)/bin/yale-haskell
	echo -n "export HASKELL=" >> $(PREFIX)/bin/yale-haskell
	echo $(SHARE) >> $(PREFIX)/bin/yale-haskell
	echo $(PREFIX)/bin/clisp-haskell >> $(PREFIX)/bin/yale-haskell
	chmod +x $(PREFIX)/bin/yale-haskell
