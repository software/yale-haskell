(make-package "MUMBLE-IMPLEMENTATION")
(load "cl-support/cl-init")

;;; Set various internal switches to appropriate values for running
;;; Haskell code.
(proclaim '(optimize (speed 3) (safety 0) (compilation-speed 0)))
(setf *compile-verbose* nil)
(setf *load-verbose* nil)
(in-package :mumble-user)
(setf *printers* '(compiling loading))
(setf *optimizers* '())
(setf *compile-interface* '#f)

;;; Load the prelude.
(compile/load *prelude-unit-filename*)

;;; Set up the saved system.
(define *saved-readtable* (lisp:copy-readtable lisp:*readtable*))

(define (haskell-toplevel)
  ;; Saved system always starts up in USER package.
  (setf lisp:*package* (lisp:find-package :mumble-user))
  ;; Saved system seems to forget about our readtable hacks.
  (setf lisp:*readtable* *saved-readtable*)
  ;; Set printer variables w/implementation-defined initial values
  ;; to known values
  (setf *print-pretty* '#f)
  (load-init-files)
  (do () ('#f)
    (cl:with-simple-restart (restart-haskell "Restart Haskell.")
      (heval))))

(define (restart-haskell)
  (cl:invoke-restart 'restart-haskell))

(ext:saveinitmem "bin/clisp-haskell"
                 :executable t
                 :quiet t
                 :init-function 'haskell-toplevel)
(ext:exit)
