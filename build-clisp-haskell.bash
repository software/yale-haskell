#!/bin/bash

dirs="ast backend cfn cl-support com command-interface csys depend derived flic import-export parser prec
      printers progs/prelude runtime support tdecl top type util"

for dir in ${dirs}; do
    mkdir -p $dir/clisp
done

export Y2=$PWD

clisp com/clisp/savesys.lisp
